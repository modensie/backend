from django.db import models


class Teacher(models.Model):
    name = models.TextField('name')


class Student(models.Model):
    name = models.TextField('name')

class Class(models.Model):
    name = models.TextField('subject')
    students = models.ManyToManyField(Student)
    teachers = models.ManyToManyField(Teacher)


    