from django.apps import AppConfig


class ModensiecoreConfig(AppConfig):
    name = 'modensieCore'
